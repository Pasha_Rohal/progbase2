#include "author.h"
#include <string>
#include <iostream>
#include<QString>
Author::Author(const QString & name,const QString & title)
    :_name(name),
     _title(title){

}
Author::Author()
    :_name(QString("AuthorName")),
     _title(QString("AuthorTitle")){

}
QString Author::getName(){
    return _name;
}
QString Author::getTitle(){
    return _title;
}
void Author::setTitle(QString & title){
    _title=title;
}
void Author::setName(QString & name){
    _name=name;
}

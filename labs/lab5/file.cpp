#include "file.h"
#include <QString>
#include <QFile>
#include <QTextStream>
#include <iostream>
#include <QDebug>
file::file()
{

}

QString file::readFile(const QString & Fname){
    QFile text(Fname);
    if (!text.open(QFile::ReadOnly|QFile::Text)){
        qDebug()<<"File "<<Fname<<" not open";
        return NULL;
    }
    QByteArray data;
    data=text.readAll();
    text.close();
    return QString(data);
}
void file::writeFile(const QString & Fname,const QString & str){
    // QString fn ="../json_xml/"+Fname;
    QFile text(Fname);
    if (!text.open(QFile::WriteOnly|QFile::Text)){
        qDebug()<<"File "<<Fname<<" not open";
        return ;
    }
    QTextStream stream(&text);
    stream<<str;
    text.close();


}

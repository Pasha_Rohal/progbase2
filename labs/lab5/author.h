#pragma once
#include <QString>
#include <iostream>
#include <QMetaType>
class Author
{
    QString _name;
    QString _title;
public:
    Author (const QString & name,const QString & title);
    Author();
    QString getName();
    QString getTitle();
    void setName(QString & name);
    void setTitle(QString & title);
};
Q_DECLARE_METATYPE(Author)
// DIRECTOR_H


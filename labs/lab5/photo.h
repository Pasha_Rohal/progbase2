#pragma once
#include <author.h>
#include <QString>
#include <QMetaType>
class Photo
{
    int _likes;
    double _repost;
    QString _category;
    Author _author;
public:
    Photo();
    Photo(const QString &category, Author author,int likes, double repost);
    QString getCategory();
    Author getAuthor();
    int getLikes();
    double getRepost();
    void setCategory(QString &category);
    void setAuthor(Author Author);
    void setLikes(int likes);
    void setRepost(double repost);
};
Q_DECLARE_METATYPE(Photo)
// FILM_H

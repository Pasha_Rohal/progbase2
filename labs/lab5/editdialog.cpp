#include "editdialog.h"
#include "ui_editdialog.h"
#include <QPushButton>
editDialog::editDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editDialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

editDialog::~editDialog()
{
    delete ui;
}
void editDialog::setEditPhoto(Photo photo){
    editPhoto=photo;
}
Photo editDialog::getEditPhoto(){
    return editPhoto;
}
void editDialog::writeEditPhoto(){
    ui->category->setText(editPhoto.getCategory());
    ui->name->setText(editPhoto.getAuthor().getName());
    ui->title->setText(editPhoto.getAuthor().getTitle());
    ui->likes->setValue(editPhoto.getLikes());
    ui->repostDoubleSpinBox->setValue(editPhoto.getRepost());

}

void editDialog::makeEditPhoto(){
    QString category =ui->category->text();
    QString dName =ui->name->text();
    QString dTitle =ui->title->text();
    int likes=ui->likes->value();
    double repost=ui->repostDoubleSpinBox->value();
    Author author(dName,dTitle);
    Photo dPhoto(category,author,likes,repost);
    editPhoto=dPhoto;
}

void editDialog::validAdd(){
    bool valid = (!ui->category->text().isEmpty()&&!ui->name->text().isEmpty()&&!ui->title->text().isEmpty());
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(valid);
}
void editDialog::on_category_textChanged(const QString &text)
{
    validAdd();
}

void editDialog::on_name_textChanged(const QString &arg1)
{
    validAdd();
}

void editDialog::on_title_textChanged(const QString &arg1)
{
    validAdd();
}


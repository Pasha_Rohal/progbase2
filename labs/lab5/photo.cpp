#include "photo.h"
#include <QString>
Photo::Photo(const QString &category, Author author,int likes, double repost)
    : _likes(likes),
      _repost(repost),
      _category(category),
      _author(author)

{

}
Photo::Photo()
    : _likes(),
      _repost(),
      _category(QString("category")),
      _author(Author())


{

}
QString Photo::getCategory(){
    return _category;
}
Author Photo::getAuthor(){
    return _author;
}
int Photo::getLikes(){
    return _likes;
}
double Photo::getRepost(){
    return _repost;
}
void Photo::setCategory(QString &category){
    _category = category;
}
void Photo::setAuthor(Author author){
    _author = author;
}
void Photo::setLikes(int likes) {
    _likes = likes;
}
void Photo::setRepost(double repost){
    _repost = repost;
}

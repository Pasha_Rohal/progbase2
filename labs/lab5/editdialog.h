#pragma once
#include "photo.h"
#include <QDialog>

namespace Ui {
class editDialog;
}

class editDialog : public QDialog
{
    Q_OBJECT

public:
    explicit editDialog(QWidget *parent = 0);
    void setEditPhoto(Photo photo);
    Photo getEditPhoto();
    void writeEditPhoto();
    void makeEditPhoto();
    void validAdd();
    ~editDialog();

private slots:
    void on_category_textChanged(const QString &text);
    void on_name_textChanged(const QString &arg1)    ;
    void on_title_textChanged(const QString &arg1);

private:
    Ui::editDialog *ui;
    Photo editPhoto;
};

// EDITDIALOG_H

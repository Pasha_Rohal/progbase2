#include "adddialog.h"
#include "ui_adddialog.h"
#include "author.h"
#include "photo.h"
#include "QMessageBox"
#include <QPushButton>

addDialog::addDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addDialog)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    //dialogFilm=Film();
}

addDialog::~addDialog()
{
    delete ui;
}

void addDialog::validAdd(){
    bool valid = (!ui->category->text().isEmpty()&&!ui->name->text().isEmpty()&&!ui->title->text().isEmpty());
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(valid);
}
void addDialog::on_category_textChanged(const QString &text)
{
    validAdd();
}

void addDialog::on_name_textChanged(const QString &arg1)
{
    validAdd();
}

void addDialog::on_title_textChanged(const QString &arg1)
{
    validAdd();
}
Photo addDialog::getDialogPhoto(){
    return this->dialogPhoto;
}
void addDialog::close(){
    close();
}
void addDialog::makeDialogPhoto(){
    QString category =ui->category->text();
    QString dName =ui->name->text();
    QString dTitle =ui->title->text();
    int likes=ui->SpinBoxforLike->value();
    double repost=ui->repostDoubleSpinBox->value();
    Author author(dName,dTitle);
    Photo dPhoto(category,author,likes,repost);
    this->dialogPhoto=dPhoto;
}


void addDialog::on_buttonBox_accepted()
{
    accept();
}

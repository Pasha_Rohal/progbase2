﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "adddialog.h"
#include <QMessageBox>
#include "photo.h"
#include <QCloseEvent>
#include <QVariant>
#include <QSharedPointer>
#include "editdialog.h"
#include <QDebug>
#include <QtAlgorithms>
#include <QAction>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFileDialog>
#include "file.h"
#include <QJsonParseError>
#include <QPixmap>
#include <QHoverEvent>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->removeButton->setDisabled(true);
    ui->editButton->setDisabled(true);

    //connect(ui->actionCreateEmptyList,SIGNAL(triggered(bool)),this,SLOT(cleanList()));
    connect(ui->actionSaveToFIle,SIGNAL(triggered(bool)),this,SLOT(saveToFile()));
    connect(ui->actionLoadFromFile,SIGNAL(triggered(bool)),this,SLOT(loadFromFile()));

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::cleanList(){
    while(ui->PhotoList->count()!=0){
        //QListWidgetItem * del=
        ui->PhotoList->takeItem(0);
        //delete del;
    }
    clearLabels();
}
void MainWindow::loadFromFile(){
    QString fileName =QFileDialog::getOpenFileName(
                this,
                tr("Open File"),
                "/home/pasha/json",
                "Text File (*.json)");
    qDebug()<<"File "<<fileName;
    file f;
    QString fromFile=f.readFile(fileName);
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(
                QByteArray::fromStdString(fromFile.toStdString()),
                &err);
    if (err.error != QJsonParseError::NoError) {
        qDebug()<<"Error in JSON file in load\n";
        return ;
    }

    QJsonObject PhotosObj = doc.object();
    QJsonArray PhotosArr = PhotosObj.value("photos").toArray();
    for (int i = 0; i < PhotosArr.size(); i++) {
        QJsonValue value = PhotosArr.at(i);
        QJsonObject PhotoObj = value.toObject();
        QString category= PhotoObj.value("category").toString();
        int likes=PhotoObj.value("likes").toInt();
        double repost=PhotoObj.value("repost").toDouble();
        QString dName= PhotoObj.value("name").toString();
        QString dTitle= PhotoObj.value("title").toString();
        Photo add(category,Author(dName,dTitle),likes,repost);
        setPhoto(add);
    }

}
void MainWindow::saveToFile(){

    QString fileName =QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                "/home/pasha/json",
                "Text File (*.json)");
    qDebug()<<"File "<<fileName;
    QJsonDocument doc;
    QJsonObject PHOTOS;
    QJsonArray photos;
    int listSize=ui->PhotoList->count();
    for(int i=0;i < listSize;i++){
        QListWidgetItem* top = ui->PhotoList->item(i);
        QVariant item = top->data(Qt::UserRole);
        Photo toSavePhoto = item.value<Photo>();

        QJsonObject photo;
        photo.insert(QString::fromStdString("category"),toSavePhoto.getCategory());
        photo.insert(QString::fromStdString("repost"),toSavePhoto.getRepost());
        photo.insert(QString::fromStdString("likes"),toSavePhoto.getLikes());
        photo.insert(QString::fromStdString("name"),toSavePhoto.getAuthor().getName());
        photo.insert(QString::fromStdString("title"),toSavePhoto.getAuthor().getTitle());
        photos.append(photo);
    }
    PHOTOS.insert("photos",photos);
    doc.setObject(PHOTOS);
    file f;
    f.writeFile(fileName,doc.toJson());

}
void MainWindow::on_addButton_clicked()
{
    addDialog dialog(this);
    Photo newPhoto;
    if(dialog.exec()==QDialog::Accepted){
        dialog.makeDialogPhoto();
        newPhoto = dialog.getDialogPhoto();
        setPhoto(newPhoto);
    }
}

void MainWindow::closeEvent(QCloseEvent * ev){
    if(QMessageBox::question(this,
                             "Exit",
                             "Are you sure?",
                             QMessageBox::Yes|
                             QMessageBox::No)==QMessageBox::Yes){
       // KittyDialog cat(this);
        //cat.setModal(true);
        //cat.exec();
        //cleanList();
        //ev->accept();
    }else{
        ev->ignore();
    }
}
void MainWindow::setPhoto(Photo/*QSharedPointer<Photo>*/&photo){
    auto photoItem = new QListWidgetItem(photo.getCategory());
    photoItem->setData(Qt::UserRole, QVariant::fromValue(photo));
    ui->PhotoList->addItem(photoItem);
}

void MainWindow::on_PhotoList_itemPressed(QListWidgetItem *item)
{
    QVariant  var=item->data(Qt::UserRole);
    Photo /*QSharedPointer<Photo>*/ photo=var.value</*QSharedPointer<Photo>*/Photo>();
    ui->categoryLineEdit->setText(photo.getCategory());
    ui->nameLineEdit->setText(photo.getAuthor().getName());
    ui->titleLineEdit->setText(photo.getAuthor().getTitle());
    ui->likesLineEdit->setText(QString::number(photo.getLikes()));
    ui->repostLineEdit->setText(QString::number(photo.getRepost()));
}

void MainWindow::on_removeButton_clicked()
{
//    QListWidgetItem * del=
    ui->PhotoList->takeItem(ui->PhotoList->currentRow());
   // delete del;
    clearLabels();
    if(ui->PhotoList->count()==0){
        ui->removeButton->setDisabled(true);
        ui->editButton->setDisabled(true);
    }
}
void MainWindow::clearLabels(){
    ui->categoryLineEdit->setText(QString(""));
    ui->nameLineEdit->setText(QString(""));
    ui->titleLineEdit->setText(QString(""));
    ui->likesLineEdit->setText(QString(""));
    ui->repostLineEdit->setText(QString(""));
}
void MainWindow::on_editButton_clicked()
{
    editDialog dialog(this);
    QListWidgetItem * del=ui->PhotoList->takeItem(ui->PhotoList->currentRow());
    QVariant item=del->data(Qt::UserRole);
    Photo toEdit=item.value<Photo>();
    dialog.setEditPhoto(toEdit);
    dialog.writeEditPhoto();
    if(dialog.exec()==QDialog::Accepted){
        dialog.makeEditPhoto();
        toEdit = dialog.getEditPhoto();
        int index=ui->PhotoList->currentRow();
        auto photoItem = new QListWidgetItem(toEdit.getCategory());
        photoItem->setData(Qt::UserRole, QVariant::fromValue(toEdit));
        ui->PhotoList->insertItem(index,photoItem);
        ui->categoryLineEdit->setText(toEdit.getCategory());
        ui->nameLineEdit->setText(toEdit.getAuthor().getName());
        ui->titleLineEdit->setText(toEdit.getAuthor().getTitle());
        ui->likesLineEdit->setText(QString::number(toEdit.getLikes()));
        ui->repostLineEdit->setText(QString::number(toEdit.getRepost()));
    }

}

void MainWindow::createNewList()
{
    ui->PhotoList->clear();
    clearLabels();
}

void MainWindow::on_PhotoList_itemActivated(QListWidgetItem *item)
{
    ui->removeButton->setDisabled(false);
    ui->editButton->setDisabled(false);
}

void MainWindow::on_executeButton_clicked()
{
    ui->ExecutableList->clear();
    int listSize=ui->PhotoList->count();

    for(int i=0;i<listSize;i++){
        QListWidgetItem* top=ui->PhotoList->item(i);
        QVariant item=top->data(Qt::UserRole);
        Photo exPhoto=item.value<Photo>();

        if(ui->executSpinBox->value() < exPhoto.getLikes()){
            auto photoItem = new QListWidgetItem(exPhoto.getCategory());
            ui->ExecutableList->addItem(photoItem);
        }
    }
}

    /*

    ui->ExecutableList->clear();
    int listSize = ui->PhotoList->count();
    qDebug() << listSize;
    QList <Photo> sort;
    for(int i = 0;i < listSize;i++){
       if()
    }

    ui->ExecutableList->clear();
    int listSize=ui->PhotoList->count();
    //qDebug()<<listSize;
    QList<Photo> sort;
    for(int i=0;i<listSize;i++){
        QListWidgetItem* top=ui->PhotoList->item(i);
        QVariant item=top->data(Qt::UserRole);
        Photo exPhoto=item.value<Photo>();
        sort.append(exPhoto);
    }
    if(ui->ExecuteSelectComboBox->currentText()=="RatingDown"){
        //https://www.linux.org.ru/forum/development/9103611

        (void)qSort(sort.begin(),sort.end(),[](Photo & a, Photo & b) { return a.getRepost() > b.getRepost(); } );
        if(ui->executSpinBox->value()<listSize){
            listSize= ui->executSpinBox->value();
        }
        for(int i=0;i<listSize;i++){
            Photo exPhoto=sort.at(i);
            auto photoItem = new QListWidgetItem(exPhoto.getCategory());
            photoItem->setData(Qt::UserRole, QVariant::fromValue(exPhoto));
            ui->ExecutableList->addItem(photoItem);
        }
    }
    else if(ui->ExecuteSelectComboBox->currentText()=="RatingUp"){
        //https://www.linux.org.ru/forum/development/9103611
        qSort(sort.begin(),sort.end(),[](Film & a, Film & b) { return a.getRating() < b.getRating(); } );//variantLessThan);
        if(ui->executSpinBox->value()<listSize){
            listSize= ui->executSpinBox->value();
        }
        for(int i=0;i<listSize;i++){
            Film exFilm=sort.at(i);
            auto filmItem = new QListWidgetItem(exFilm.getName());
            filmItem->setData(Qt::UserRole, QVariant::fromValue(exFilm));
            ui->ExecutableList->addItem(filmItem);
        }
    }else if(ui->ExecuteSelectComboBox->currentText()=="CollectionDown"){
        //https://www.linux.org.ru/forum/development/9103611
        qSort(sort.begin(),sort.end(),[](Film & a, Film & b) { return a.getCollection() > b.getCollection(); } );//variantLessThan);
        if(ui->executSpinBox->value()<listSize){
            listSize= ui->executSpinBox->value();
        }
        for(int i=0;i<listSize;i++){
            Film exFilm=sort.at(i);
            auto filmItem = new QListWidgetItem(exFilm.getName());
            filmItem->setData(Qt::UserRole, QVariant::fromValue(exFilm));
            ui->ExecutableList->addItem(filmItem);
        }
    }else if(ui->ExecuteSelectComboBox->currentText()=="CollectionUp"){
        //https://www.linux.org.ru/forum/development/9103611
        qSort(sort.begin(),sort.end(),[](Film & a, Film & b) { return a.getCollection() < b.getCollection(); } );//variantLessThan);
        if(ui->executSpinBox->value()<listSize){
            listSize= ui->executSpinBox->value();
        }
        for(int i=0;i<listSize;i++){
            Film exFilm=sort.at(i);
            auto filmItem = new QListWidgetItem(exFilm.getName());
            filmItem->setData(Qt::UserRole, QVariant::fromValue(exFilm));
            ui->ExecutableList->addItem(filmItem);
        }
        */




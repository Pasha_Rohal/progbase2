#pragma once
#include <photo.h>
#include <QDialog>

namespace Ui {
class addDialog;
}

class addDialog : public QDialog
{
    Q_OBJECT

public:
    void validAdd();
    Photo getDialogPhoto();
    void makeDialogPhoto();
    //    void closeEvent(QCloseEvent * ev);
    void close();
    explicit addDialog(QWidget *parent = 0);
    ~addDialog();

private slots:

    void on_category_textChanged(const QString &text);
    void on_name_textChanged(const QString &arg1);
    void on_title_textChanged(const QString &arg1);
    //void on_buttonBox_accepted();

    void on_buttonBox_accepted();

private:
    Ui::addDialog *ui;
    Photo dialogPhoto;
};

// ADDDIALOG_H

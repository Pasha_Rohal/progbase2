
#include <QMainWindow>
#include <QCloseEvent>
#include "photo.h"
#include <memory>
#include <QSharedPointer>
#include <QListWidgetItem>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void closeEvent(QCloseEvent * ev);
    void setPhoto(Photo &photo);
    void clearLabels();
    ~MainWindow();

private slots:
    void cleanList();
    void on_addButton_clicked();
    void saveToFile();
    void loadFromFile();
    void on_PhotoList_itemPressed(QListWidgetItem *item);

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void createNewList();

    void on_PhotoList_itemActivated(QListWidgetItem *item);

    void on_executeButton_clicked();

private:
    Ui::MainWindow *ui;
};
// MAINWINDOW_H

#include <stdio.h>
#include <stdlib.h>

#include <stdbool.h>
#include <list.h>
#include <csv.h>
#include <string_buffer.h>
#include <strdup.h>






CsvRow * CsvRow_new(void){
    CsvRow * self = malloc(sizeof(CsvRow));
    self->value = List_new();
    return self;
}

void CsvRow_free(CsvRow * self){
    List_free(self->value);
    free(self);
}
void CsvRow_add(CsvRow * self, void * value){
    List_add(self->value,value);
}

CsvTable * CsvTable_new(void){
    CsvTable * self = malloc(sizeof(CsvTable));
    self->rows = List_new();
    return self;
}

void CsvTable_free(CsvTable * self){
    List_free(self->rows);
    free(self);
}
void CsvTable_add (CsvTable * self, CsvRow * row){
    List_add(self->rows, (void *)row);
}

CsvRow* CsvTable_at(CsvTable * self, int index)
{
    return (CsvRow*)List_at(self->rows, index);
}

int CsvTable_size(CsvTable * self)
{
    return self->rows->length;
}

static void List_CopyTo(List *self, List *dest){
    int count = List_count(self);
    for(int i = 0; i < count; i++){
        void *value = List_at(self, i);
        List_add(dest, value);
    }
}

void CsvTable_rows(CsvTable * self, List * rows){
    List_CopyTo(self->rows, rows);
}

CsvTable * CsvTable_newFromString(const char * csvString)
{   
    CsvTable * t=CsvTable_new();
    CsvRow * row = CsvRow_new();
    StringBuffer * value =StringBuffer_new();
    while(1){
        char ch=*csvString;
        if(ch == '\0'){
            CsvRow_add(row,StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            CsvTable_add(t,row);
            break;
        } else if(ch ==','){
            CsvRow_add(row,StringBuffer_toNewString(value));
            StringBuffer_clear(value);
        }else if(ch =='\n'){
            CsvRow_add(row,StringBuffer_toNewString(value));
            StringBuffer_clear(value);
            CsvTable_add(t,row);
            row=CsvRow_new();
        }
        else{
            StringBuffer_appendChar(value,ch);
        }
        csvString++;
    }
    StringBuffer_free(value);
    return t;
}

char * CsvTable_toNewString  (CsvTable * self)
{
    StringBuffer *sb = StringBuffer_new();

    for(int ri = 0; ri < List_count(self->rows); ri++)
    {
        if(ri != 0)
        {
            StringBuffer_appendChar(sb, '\n');
        }
        CsvRow *row = List_at(self->rows, ri);
        int nvalues = List_count(row->value);

        for(int vi = 0; vi < nvalues; vi++)
        {
            if(vi != 0)
            {
                StringBuffer_appendChar(sb, ',');
            }
            char * value = List_at(row->value, vi);
            StringBuffer_append(sb, value);
        }
    }
    char * result = StringBuffer_toNewString(sb);    
    return result;
}




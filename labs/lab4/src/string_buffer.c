#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <string_buffer.h>
#include <assert.h>
#include <strdup.h>

static const int INITIAL_CAPACITY = 256;

struct __StringBuffer
{
    char *buffer;
    size_t capacity;
    size_t length;
};
StringBuffer *StringBuffer_new(void)
{
    StringBuffer *self = malloc(sizeof(struct __StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->buffer = malloc(sizeof(char) * self->capacity);
    StringBuffer_clear(self);
return self;
}
void StringBuffer_free(StringBuffer *self)
{
    free(self->buffer);
    free(self);
}

static void ensureCapacity(StringBuffer *self, int appendLength)
{
    while (self->length + appendLength > self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        char *newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
        if (newBuffer == NULL)
        {
            fprintf(stderr,"Out of memory");
            abort();
        }
        else
        {
            self->buffer = newBuffer;
            self->capacity = newCapacity;
        }
    }
}
void StringBuffer_append(StringBuffer *self, const char *str)
{
    size_t strLen = strlen(str);
    ensureCapacity(self, strLen);
    strcat(self->buffer + (self->length - 1), str);
    self->length += strLen;
}

void StringBuffer_appendChar(StringBuffer *self, char ch)
{
    //cheak realloc
    ensureCapacity(self,1);
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}
void StringBuffer_appendFormat(StringBuffer *self, const char *fmt, ...)
{
    assert(0 && "Not implemented");
}

void StringBuffer_clear(StringBuffer *self)
{
    self->buffer[0] = '\0';
    self->length = 1;
}
char *StringBuffer_toNewString(StringBuffer *self)
{
   char * str = self->buffer;
    return strdup(str);
}



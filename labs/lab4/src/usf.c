#include <stdio.h>
#include <stdlib.h>

#include <usf.h>
void mainMenu()
{
    printf("------------------------------------------->\n");
    printf("1. Create new photo\n");
    printf("2. Remove photo\n");
    printf("3. Change all characters about this photo\n");
    printf("4. Change some information in photo\n");
    printf("5. Print photo likes been less than x\n");
    printf("6. Save To File\n");
    printf("7. Print photo\n");
    printf("0. Exit\n");
    printf("------------------------------------------->\n");
}

void checkInt(int x){
    do {
      x = scanf("%d", &x);
      while (getchar() != '\n');
        if (x == 1) printf("\n");
        else printf("%s", "try again:(\n"); 
    }
    while (x != 1);
}
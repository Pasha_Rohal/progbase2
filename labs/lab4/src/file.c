#include <stdlib.h>
#include <stdio.h>

#include <file.h>

bool file_exists(const char *file_name)
{
    FILE *f = fopen(file_name, "rb");
    if (!f)
    {
        return false;
    }   
    fclose(f);
    return true;
}

size_t get_file_size(const char *file_name)
{
    FILE *f = fopen(file_name, "rb");
    if (!f)
    {
        return 0;
    }
    fseek(f, 0, SEEK_END);
    size_t fsize = ftell(f);
    fclose(f);
    return fsize;
}

size_t read_file_to_buffer(const char *file_name, char *buffer, long buffer_length)
{
    FILE *f = fopen(file_name, "rb");
    if (!f)
    {
        return 0;
    }
    size_t read_bytes = fread(buffer, 1, buffer_length, f);
    fclose(f);
    return read_bytes;
}

void open_file_stream(FILE *file, const char *file_name)
{
    file = freopen(file_name, "w", stdout);
    if (!file)
    {
        exit(EXIT_FAILURE);
    }
}

void close_file_stream(FILE *file)
{
    if (file)
    {
        fflush(file);
        fclose(file);
    }
}

bool write_text_to_file(const char *text, const char *file_name)
{
    FILE *file = fopen(file_name, "w");
    if (!file)
    {
        return false;
    }
    else
    {
        fputs(text, file);
        fflush(file);
        fclose(file);
    }
    return true;
}
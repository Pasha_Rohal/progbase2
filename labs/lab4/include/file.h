#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <string_buffer.h>
#include <strdup.h>

int fileExists(const char *fileName);
long getFileSize(const char *fileName);
int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);


bool file_exists(const char *file_name);


size_t get_file_size(const char *file_name) ;


size_t read_file_to_buffer(const char *file_name, char *buffer, long buffer_length) ;

void open_file_stream(FILE *file, const char *file_name) ;


void close_file_stream(FILE *file) ;


bool write_text_to_file(const char *text, const char *file_name) ;

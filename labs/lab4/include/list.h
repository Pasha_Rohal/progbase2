#pragma once

#include <stdio.h>
#include <stdlib.h>

typedef struct __NumList{
    void ** item;
    int capasity;
    int length;
} List;


List * List_new(void);
void List_free(List * self);

void List_add(List * self, void * value);
void List_insert(List * self, size_t index, void * value);
void * List_at(List * self, size_t index);
void * List_set(List * self, size_t index, void * value);
void * List_removeAt(List * self, size_t index);
size_t List_count(List * self);
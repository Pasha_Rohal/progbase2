#pragma once
#include <list.h>
#include <string_buffer.h>
typedef struct __CsvTable {
    List * rows;
} CsvTable;

typedef struct __CsvRow {
    List * value;
} CsvRow;

CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow * self, void * value);
void CsvRow_values(CsvRow * self, List * values);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
CsvRow* CsvTable_at(CsvTable * self, int index);
int CsvTable_size(CsvTable* self);

CsvTable * CsvTable_newFromString(const char * csvString);

char * CsvTable_toNewString  (CsvTable * self);






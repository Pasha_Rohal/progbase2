#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <progbase/console.h>
#include <progbase.h>
#include <ctype.h>
#include <progbase.h>
#include <progbase/console.h>

#include <file.h>
#include <csv.h>
#include <list.h>
#include <usf.h>

#define CATEGORY_LENGTH 30
#define NAME_LENGTH 30


enum PhotoEnum{
    NAME,       
    CATEGORY,   
    LIKES,     
    PERSENT_LOVE,
    REPOST 
    };

typedef struct Photo{
    char name[NAME_LENGTH]; 
    char category[CATEGORY_LENGTH];         
    int likes ;            
    double persent_love;
    int repost;            
} Photo;


void photoToCsv(Photo* photo, CsvRow * row);
CsvTable* readPhotoToNew(const char* fileName);
void rowPhoto(CsvRow* row, Photo* photo);
void stringPhoto(Photo * photo, char buf[200]);
void charToPhoto(char  buf[200], Photo * photo);
void change(CsvRow * photo, char field[50], char value[50]);
void likesX(CsvTable* photos, int x);

void printPhoto(Photo* photo);    
void compiller(void);
void mainMenu();

void workspace(CsvTable * photos, int c);

void printPhotos(CsvTable* photos);

int main(int argc, char *argv[])
{
    compiller();
    return 0;
}

void photoToCsv(Photo* photo, CsvRow * row)
{
    char* name = (char *)malloc(NAME_LENGTH * sizeof(char));
    strcpy(name, photo->name);
    char * category = (char*)malloc(CATEGORY_LENGTH * sizeof(char));
    strcpy(category, photo->category); 
    int * likes = (int *)malloc(sizeof(int));
    double * persent_love = (double*)malloc(sizeof(double));
    int * repost = (int *)malloc(sizeof(int));

    strcpy(name, photo->name);
    strcpy(category,photo->category);
    *likes = photo->likes;
    *persent_love = photo->persent_love;
    *repost = photo->repost;

    CsvRow_add(row, (void*)name);
    CsvRow_add(row, (void*)category);
    CsvRow_add(row, (void*)likes);
    CsvRow_add(row, (void*)persent_love);
    CsvRow_add(row, (void*)repost);
}

CsvTable* readPhotoToNew(const char* fileName)
{
    CsvTable* creater = CsvTable_new();
    FILE * f = fopen(fileName, "r");
    if (f == 0){
        printf("EMPTY");
        abort();
}
    char  buf[200];
    Photo tmpInsta;
    CsvRow* tempFunc;
    while (fgets(buf, 200, f))
    {
        charToPhoto(buf, &tmpInsta);
        tempFunc = CsvRow_new();
        photoToCsv(&tmpInsta, tempFunc);
        CsvTable_add(creater, tempFunc);
    }
    fclose(f);
    return creater;
}

void rowPhoto(CsvRow* row, Photo* photo)
{
    strcpy(photo->name, (char*)List_at(row->value, NAME));
    strcpy(photo->category,(char*)List_at(row->value, CATEGORY));
    photo->likes = *((int*)List_at(row->value, LIKES));
    photo->persent_love = *((double*)List_at(row->value, PERSENT_LOVE));
    photo->repost = *((int*)List_at(row->value, REPOST));
}

void likesX(CsvTable* photos, int x)
{
    for (int i = 0; i < List_count(photos->rows); i++)
    {
        CsvRow* tempFunc = CsvTable_at(photos, i);
        if (*(int*)List_at(tempFunc->value, LIKES) < x)
        {
            Photo temp;
            rowPhoto(tempFunc, &temp);
            printPhoto(&temp);
        }
    }
}

void lookingPhoto(CsvTable* photos)
{
    int x;
    printf("Input X: ");
    scanf("%i",&x);
    likesX(photos, x);
}
void printPhoto(Photo* photo)
{
    char  buf[200];
    stringPhoto(photo, buf);
    printf("%s", buf);
}


void ChangeElement(CsvTable* photos)
{
    unsigned int i;
    size_t tmpNum = CsvTable_size(photos);
    printf("print line index: ");
    if(scanf("%x", &i) == 1){
        if ((i < 0) || (i >= tmpNum))
        {
            Console_setCursorAttribute(BG_CYAN);        
            printf("you enter wrong,Try later!");
            Console_setCursorAttribute(BG_DEFAULT);
            return;
        }
    }    
    char  field[100];
    char  newValue[200];
    printf("Input : \nname\ncategory\nlikes\npersent_love\nrepost\n:");
    scanf("%s", field);
    printf("Input changer: ");
    scanf("%s", newValue);
    change(CsvTable_at(photos, i), field, newValue);
}

void  stringPhoto(Photo * photo, char buf[200])
{
    sprintf(buf, "%s,%s,%i,%1.f,%i\n", photo->name, photo->category, photo->likes,photo->persent_love,photo->repost);
}

void  charToPhoto(char buf[200], Photo * photo)
{
    sscanf(buf, "%s,%s,%i,%2lf,%i", photo->name, photo->category, &photo->likes,&photo->persent_love,&photo->repost);
}


void change(CsvRow * photo, char field[50], char value[50])
{
    if (strcmp(field, "name") == 0) strcpy((char*)List_at(photo->value,NAME), value);
    else if (strcmp(field, "category") == 0) sscanf(value, "%s", (char*) List_at(photo->value, CATEGORY));
    else if (strcmp(field, "likes") == 0) sscanf(value, "%i", (int*)List_at(photo->value, LIKES));
    else if (strcmp(field, "persent_love") == 0) sscanf(value, "%lf", (double*)List_at(photo->value, PERSENT_LOVE));
    else if (strcmp(field, "repost") == 0) sscanf(value, "%i", (int*)List_at(photo->value, REPOST));
    return;
}

void SaveToFile(CsvTable *photos)
{
    char filename[50];
    printf("Input file name: ");
    scanf("%s", filename);
    FILE * f = fopen(filename, "w");

    char buf[200];
    for (int i = 0; i < CsvTable_size(photos); i++)
    {
        CsvRow* temp_row = CsvTable_at(photos, i);
        Photo temp;
        rowPhoto(temp_row, &temp);
        stringPhoto(&temp, buf);
        fputs(buf, f);
    }
    fclose(f);
}

void replace(CsvTable * photos)
{
    unsigned int i;
    printf("enter index line : ");
    if(scanf("%x", &i + 1) == 1){
        if ((i < 0) || (i >= CsvTable_size(photos)))
        {
            Console_setCursorAttribute(BG_CYAN);
            printf("you enter wrong,Try later!");
            Console_setCursorAttribute(BG_DEFAULT);
        return;
        }
    }
    CsvRow* changePhoto = CsvTable_at(photos, i);

    printf("Photo name: "); scanf("%s", (char*) List_at(changePhoto->value,NAME));
    printf("Photo category: "); scanf("%s",(char*) List_at(changePhoto->value, CATEGORY));
    printf("Photo likes: "); scanf("%i",(int*) List_at(changePhoto->value, LIKES));
    printf("Photo persent_love: ");scanf("%lf",(double *) List_at(changePhoto->value, PERSENT_LOVE));
    printf("Photo repost: "); scanf("%i",(int*) List_at(changePhoto->value, REPOST));
}

void removeIndex(CsvTable * photos)
{   
    unsigned int i;
    printf("print index photo: ");
    if(scanf("%x",&i) == 1){
        if ((i < 0) || (i >= CsvTable_size(photos)))
        {
            Console_setCursorAttribute(BG_CYAN);
            printf("\nyou enter wrong,Try later!\n");
            Console_setCursorAttribute(BG_DEFAULT);
            return;
        }
    }

    List_removeAt(photos->rows,i);
}

void createPhoto(CsvTable* photos)
{
    Photo temp;
    printf("Photo name:");(scanf("%s", temp.name));
  
    printf("Photo category:");(scanf("%s", temp.category));
    
    printf("Photo likes:");(scanf("%d", &temp.likes));
    
    printf("Photo persent_love:");(scanf("%lf", &temp.persent_love));
    if(temp.persent_love < 0 || temp.persent_love > 100){
        printf("\nrepost only + number\n");
        List_removeAt(photos->rows,CsvTable_size(photos));
        return;
    }

    printf("Photo repost:");if(!scanf("%i", &temp.repost));
        if(temp.persent_love < 0){
        printf("\npersent only < 100\n");
        List_removeAt(photos->rows,CsvTable_size(photos));
        return ;
    }
    CsvRow* newPhoto = CsvRow_new();
    photoToCsv(&temp, newPhoto);
    CsvTable_add(photos, newPhoto);

}
void compiller(void)
{
    Console_clear();
    printf("1. Create new file\n");
    printf("2. Load file\n");
    int choise;
    printf(": ");
    scanf("%d", &choise);
    CsvTable * photos =  NULL ;
    if (choise == 1)
    {
        photos = CsvTable_new();
    }
    else if (choise == 2)
    { 
        char  fileName[100] = "";
        printf("file name: ");
        scanf("%s", fileName);
        photos = readPhotoToNew(fileName);
    }
    do
    {
        mainMenu();
        scanf("%d", &choise);
        workspace(photos, choise);
        Photo temp;
    } while (choise != 0);

    CsvTable_free(photos);
    photos = NULL;
}


void workspace(CsvTable * photos, int c)
{
    switch (c)
    {
    case 1:
        createPhoto(photos);
        Console_setCursorAttribute(BG_CYAN);
        getchar();
        Console_setCursorAttribute(BG_DEFAULT);
        Console_clear();
        break;
    case 2:
        Console_clear();
        removeIndex(photos);
        printPhotos(photos);
        getchar();
        break;
    case 3:
        Console_clear();
        replace(photos);
        printPhotos(photos);
        Console_clear();
        printPhotos(photos);
        break;
    case 4:
        Console_clear();
        ChangeElement(photos);
        Console_clear();
        printPhotos(photos);
        getchar();
        break;
    case 5:
        Console_clear();
        lookingPhoto(photos);
        break;
    case 6:
        SaveToFile(photos);
        Console_clear();
        break;
    case 7:
        Console_clear();
        printPhotos(photos);
        getchar();
        break;
    }
}
void printPhotos(CsvTable* photos)
{

    Photo temp;
    for (int i = 0; i < CsvTable_size(photos); ++i)
    {
        printf("[%i]--->",i);
        rowPhoto(List_at(photos->rows, i),&temp);
        printPhoto(&temp);
    }
}



#include "numlist.h"
#include <stdio.h>
#include <assert.h>
#define NDEBUG

 struct __NumList
{
  int *array;
  int capacity;
  int length;
};

void NumList_free(NumList *self)
{
  if(self==NULL)
{
  printf("Null pointer exeption");
  abort();
}
  free(self->array);
  free(self);
}

NumList *NumList_new(void)
{
  NumList *self = malloc(sizeof(NumList)); //cheak result;
  if (self == NULL)
    {
      printf("Out of memory");
      abort();
    }
  self->length = 0;
  self->capacity = CAPACITY;
  self->array = malloc(sizeof(int) * CAPACITY);    //check for null result;

    return self; 
}

int NumList_removeAt(NumList *self, size_t index) {
  if(self==NULL){
    printf("Out of memory");
    abort();
  }
  if(self->array[index] < 0 && self->array[index] >= self->length){
  assert(0 && "Invalid index");
  }
  NumList_at(self,index);
  for(int i=index;i<self->length-1;i++){
      self->array[i]=self->array[i+1];
  }
  self->length--;
  return self->length;
}

void NumList_add(NumList *self, int value)
{
  if(self==NULL){
  printf("Null pointer exeption");
  abort();
}
  if (self->capacity > self->length)
  {
    self->array[self->length] = value;
    self->length++;
  }
}

int NumList_at(NumList * self, size_t index){
  if(self->array[index] < 0 && self->array[index] >= self->length){
  assert(0 && "Invalid index");
  }
  assert(index<self->length);
  return self->array[index];
}
void NumList_insert(NumList *self, size_t index, int value)
{
  if (self == NULL)
  {
    printf("Null pointer exeption");
    abort();
  }
  if(self->array[index] < 0 && self->array[index] >= self->length){
    assert(0 && "Invalid index");
  }
  if (self->length > self->capacity)
  {
    self->array = realloc(self->array, sizeof(int) * self->capacity * 2);
    if (self->array == NULL){
      printf("Out of memory");
      abort();       
      }
      self->capacity = self->capacity * 2;
    }

  for (int i = self->length - 1; i >= index; i--){
      self->array[i + 1] = self->array[i];
    }
    self->array[index] = value;
    self->length++;
}

int NumList_set(NumList * self, size_t index, int value){
  if(self==NULL){
  printf("Out of memory");
  abort();
}
  if(self->array[index] < 0 && self->array[index] >= self->length){
    assert(0 && "Invalid index");
  }
  NumList_at(self,index);
  self->array[index] = value;
  return value;
}

size_t NumList_count(NumList * self){
  if(self==NULL){
  printf("Null pointer exeption");
  abort();
  }
  return self->length;
}

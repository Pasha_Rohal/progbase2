#include <stdlib.h>
#include <stdio.h>
#include "numlist_ext.h"

struct __NumList
{
  int *array;
  int capacity;
  int length;
};

void NumList_Delete(NumList * self){
    for(int i=0;i<self->length;i++){
        if(self->array[i] < 0){
            NumList_removeAt(self,i);
            i--;
        }
    }
}

void NumList_print(NumList *self){
    if(self->length<=self->capacity && self->length > 0){
        for(int i=0;i<self->length;i++){
            printf("%i,",self->array[i]);
        }
    }
}
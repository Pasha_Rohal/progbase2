#include <stdlib.h>
#include <stdio.h>
#include <progbase/console.h>
#include "numlist.h"
#include <time.h>

#define NDEBUG

int main(void) {
  srand(time(0));
  NumList * self=NumList_new();
  for(int i=0;i<CAPACITY;i++){
    NumList_add(self,rand()%(201)-100);
  }
    NumList_print(self);
    NumList_Delete(self);
    puts(" ");
    NumList_print(self);
    NumList_free(self);
    return 0;
}
#pragma once

#include <stdlib.h>
#include <stdio.h>


typedef struct __NumList NumList;
static const int CAPACITY = 10;

NumList * NumList_new(void);
void NumList_free(NumList * self);
void NumList_add(NumList * self, int value);
void NumList_insert(NumList * self, size_t index, int value);
int NumList_removeAt(NumList * self, size_t index);
int NumList_at(NumList * self, size_t index);
int NumList_set(NumList * self, size_t index, int value); //на первну позицію передає значення.
size_t NumList_count(NumList * self);
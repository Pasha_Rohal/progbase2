#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <progbase/events.h>
#include <progbase/console.h>
#include <progbase.h>
#include <unistd.h>


/* custom constant event type ids*/
enum {
	KeyInputEventTypeId,
	RandomNumberEventTypeId,
};

/* event handler functions prototypes */

void UpdatePrintHandler_update(EventHandler * self, Event * event);
void KeyInputHandler_update(EventHandler * self, Event * event);
void CustomHandler_handleEvent(EventHandler * self, Event * event);
void Timer_handleEvent(EventHandler * self, Event * event);

typedef struct Timer Timer;
struct Timer {
	int id;
	int timeCounter;
    int N;
};

int main(void) {
	// startup event system
	EventSystem_init();

	// add event handlers
	Timer * timer = malloc(sizeof(Timer));
	puts("Створити обробник-таймер, який з періодичністю N мілісекунд буде\nвикликати функцію задану вказівником. Таймер може реагувати на\nподії зупинки і продовження виконання таймера, які генеруються\nіншими обробниками\n(наприклад, на натискання певних кнопок).");
	
	Console_setCursorAttribute(BG_CYAN);
	puts("if you want  stop or continue the timer click <<w>>");
	Console_setCursorAttribute(BG_DEFAULT);
	Console_setCursorAttribute(BG_GREEN);
	puts("finish work program => <<q>>");
	Console_setCursorAttribute(BG_YELLOW);
	puts("enter a few milliseconds");
	
    Console_setCursorAttribute(BG_DEFAULT); 
	    if(scanf("%i",&timer->N) == 0){
            printf("\ntry again\n");
            return EXIT_FAILURE;
        }
	timer->id = timer->N;
	timer->timeCounter = 0;
	EventSystem_addHandler(EventHandler_new(NULL, NULL, UpdatePrintHandler_update));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, KeyInputHandler_update));
	EventSystem_addHandler(EventHandler_new(timer, free, Timer_handleEvent));
	EventSystem_addHandler(EventHandler_new(NULL, NULL, CustomHandler_handleEvent));

	// start infinite event loop
	EventSystem_loop();
	// cleanup event system
	EventSystem_cleanup();
	return 0;
}

/* event handlers functions implementations */

void UpdatePrintHandler_update(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			Console_setCursorAttribute(BG_CYAN);
			puts("if you want  stop or continue the timer click <<w>>");
			Console_setCursorAttribute(BG_DEFAULT);
			Console_setCursorAttribute(BG_GREEN);
			puts("finish work program => <<q>>");
			Console_setCursorAttribute(BG_DEFAULT);
			puts("");
			puts("<<<START!>>>");
			puts("");
			break;
		}
		case UpdateEventTypeId: {
			putchar('>');
			break;
		}
		case ExitEventTypeId: {
			puts("");
			puts("<<<EXIT!>>>");
			puts("");
			break;
		}
	}
}

void KeyInputHandler_update(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {  // non-blocking key input check
		char * keyCode = malloc(sizeof(char));
		*keyCode = getchar();
		EventSystem_emit(Event_new(self, KeyInputEventTypeId, keyCode, free));
	}
}

void CustomHandler_handleEvent(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			srand(time(0));
			break;
		}
		case RandomNumberEventTypeId: {
			int * number = (int *)event->data;
			printf("Random number %i\n", *number);   
		}
		case KeyInputEventTypeId: {
			char * keyCodePtr = (char *)event->data;
			char keyCode = *keyCodePtr;
			if (keyCode == 'q') {
				EventSystem_exit();
				break;
			}
            if(keyCode == 'w'){
                Console_setCursorAttribute(BG_CYAN);
				puts("\nif you want continue the timer print <<w>>\n");
				Console_setCursorAttribute(BG_GREEN);
				puts("or you can finish if print <<q>>\n");
				 Console_setCursorAttribute(BG_DEFAULT);
                while(1){
					char n = Console_getChar();
					//scanf("%c",&n); 
					if(n == 'q'){
						EventSystem_exit(); 
						break;
					}
					if(n == 'w'){
						return;
					}	 
					
				}
			}	
		}
	} 
}

void Timer_handleEvent(EventHandler * self, Event * event) {
	switch(event->type) {
		case UpdateEventTypeId: {
			Timer * timer = (Timer *)self->data;
			timer->timeCounter +=1;
            timer->id -=1;
            sleepMillis(100);
			if (timer->timeCounter % 10 == 0) {
			    int lou = timer->timeCounter;
				lou /=10;
				printf("\nTimer : %i {sec} \n", lou);  
			}
            if(timer->id == 0){
                int * number = malloc(sizeof(int));
				*number = rand() % 200 - 100;
				EventSystem_emit(Event_new(self, RandomNumberEventTypeId, number, free));
                timer->id=timer->N;
            }
		}
	}
}



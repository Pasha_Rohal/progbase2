#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <bintree.h>
#include <stdio.h>

typedef struct __BSTree BSTree;


BSTree * BSTree_new(void);
void BSTree_free(BSTree * self);

void BSTree_insert(BSTree * self, int key);
void mainInsert(BinTree *self, int key);

void BSTree_clear(BSTree * self);

// extra

void BSTree_printFormat(BSTree * self);
void BSTree_printTraverse(BSTree * self); 
void printLevel(BinTree * node,int level);
   
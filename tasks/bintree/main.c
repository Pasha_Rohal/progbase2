#include <stdio.h>
#include <stdlib.h>
#include <bintree.h>
#include <bstree.h>
#include <time.h>

int main(void)
{   
    int r;
    BSTree *node=BSTree_new();
    int array[]={-15,12,-3,11,-8,7,-2};
    for(int i=0;i<7;i++){
      r=array[i];
      BSTree_insert(node, r);
    }
    BSTree_printFormat(node);
    BSTree_printTraverse(node);
    BSTree_free(node);
}
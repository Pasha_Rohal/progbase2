#include <bintree.h>
#include <stdlib.h>
#include <stdio.h>

BinTree * BinTree_new(int value){
    BinTree*self=malloc(sizeof(BinTree));
    self->value=value;
    self->left=NULL;
    self->right=NULL;
    return self;
}

BinTree_free(BinTree * self){
    if(self==NULL){
        return;
    }
    BinTree_free(self->left);
    BinTree_free(self->right);
    free(self);    
}
#include <bintree.h>
#include <bstree.h>
#include <stdlib.h>
#include <stdio.h>

struct __BSTree {
    BinTree * root;
};

BSTree * BSTree_new(void){
    BSTree *self = malloc(sizeof(BSTree));
    self->root = NULL;
}
void BSTree_free(BSTree * self){
    if(self==NULL){ 
        return;
    }
    BSTree_clear(self);
    free(self);
}
void printLevel(BinTree * node,int level){
    for(int i=0;i<level;i++){
        printf("..");
    }
    if(node==NULL){
        printf("NULL\n");
        return;   
    }
    printf("%i\n",node->value);
    if(node->left || node->right){
    printLevel(node->left,level+1);
    printLevel(node->right,level+1);
    }
}

void BSTree_clear(BSTree *self)   //cheak
{
    if(self==NULL){
        return;
    }
    BinTree_free(self->root);
}
void BSTree_printFormat(BSTree *self)
{
    printLevel(self->root, 0);
}
static void inorder(BinTree *node){
    if (node == NULL){
        return;
    }
    inorder(node->left);
    printf("%d ", node->value);  
    inorder(node->right);
}

 void mainInsert(BinTree *self, int key){
     if (key < self->value){
        if (self->left == NULL){
            self->left = BinTree_new(key);
        }
        else{
            mainInsert(self->left, key);
        }
    }
    else{
        if (self->right == NULL){
            self->right = BinTree_new(key);
        }
        else{
            mainInsert(self->right, key);
        }
    }
}
void BSTree_insert(BSTree * self, int key){
    if(self==NULL){
        printf("empty");
    }
    if (self->root != NULL){
        mainInsert(self->root, key);
    }
    else{
        self->root = BinTree_new(key);
    }
}
void BSTree_printTraverse(BSTree *self)
{
    if (self->root==NULL){
        printf("empty");
    }
    inorder(self->root);
    printf("\n");
}



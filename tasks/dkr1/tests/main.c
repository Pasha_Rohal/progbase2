
#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <list.h>
#include <lexer.h>

START_TEST (split_oneNumber_oneNumberToken4)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens(">", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_BOLSHE, ">" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken5)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("<=", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_MENSHERAVNO, "<=" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST


START_TEST (split_empty_empty)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 0);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("13", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "13" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken2)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("15", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_NUMBER, "15" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST


START_TEST (split_onePow_onePowToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("**", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_DOUBLEPOW, "**" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_onePow_oneRavnoToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("=", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_RAVNO, "=" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST



START_TEST (split_onePow_oneWhileToken)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("while", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_WHILE, "while" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_onePow_oneWhileToken2)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("&&", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_AND, "&&" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST

START_TEST (split_onePow_oneWhileToken3)
{
   List * tokens = List_new();
   int status = Lexer_splitTokens("if", tokens);
   ck_assert_int_eq(status, 0);
   ck_assert_int_eq(List_count(tokens), 1);
   Token * firstToken = (Token *)List_at(tokens, 0);
   Token testToken = { TokenType_IF, "if" };
   ck_assert(Token_equals(firstToken, &testToken));
   Lexer_clearTokens(tokens);
   List_free(tokens);
}
END_TEST



/*
  @tod
o test other cases
*/
Suite *test_suite() {
 Suite *s = suite_create("Lexer");
 TCase *tc_sample = tcase_create("SplitTest");
 //
 tcase_add_test(tc_sample, split_empty_empty);
 tcase_add_test(tc_sample, split_onePow_oneRavnoToken);
 tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
 tcase_add_test(tc_sample, split_oneNumber_oneNumberToken2);
 tcase_add_test(tc_sample, split_oneNumber_oneNumberToken4);
 tcase_add_test(tc_sample, split_oneNumber_oneNumberToken5);
 tcase_add_test(tc_sample, split_onePow_onePowToken);
 tcase_add_test(tc_sample, split_onePow_oneWhileToken);
 tcase_add_test(tc_sample, split_onePow_oneWhileToken2);
 tcase_add_test(tc_sample, split_onePow_oneWhileToken3);

 //
 suite_add_tcase(s, tc_sample);
 return s;
}

int main(void) {
 Suite *s = test_suite();
 SRunner *sr = srunner_create(s);
 srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
 srunner_run_all(sr, CK_VERBOSE);

 int num_tests_failed = srunner_ntests_failed(sr);
 srunner_free(sr);
 return num_tests_failed;
}
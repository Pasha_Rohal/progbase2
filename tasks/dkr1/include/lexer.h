#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <iterator.h>

#include <list.h>

typedef enum {
   TokenType_PLUS,//1
   TokenType_MINUS,//3
   TokenType_PERSENT,//5
   TokenType_DILENYA,//6
   TokenType_BLOCKOPEN,//7
   TokenType_BLOCKCLOSE,//8
   TokenType_ARRAYOPEN,//9
   TokenType_ARRAYCLOSE,//10
   TokenType_OPERETCLOSE,//11
   TokenType_OPERETOPEN,//12
   TokenType_ENDLINEPROGRAME,//13
   TokenType_COMMA,//14
   TokenType_DOUBLEPOW,//15
   TokenType_RAVENSTVO,//16
   TokenType_AND,//17
   TokenType_AMPERSANT,//18
   TokenType_OTRITSANIE,//
   TokenType_NOT_RAVNO,//
   TokenType_MENSHERAVNO,//
   TokenType_MENSHE,//
   TokenType_BOLSHE,//
   TokenType_BOLSHERAVNO,//
   TokenType_STRING,//
   TokenType_RED,//
   TokenType_IF,//
   TokenType_ELSE,//
   TokenType_BOOL,//
   TokenType_ID,//30
   TokenType_NUMBER, //
   TokenType_POW,//
   TokenType_OR,//
   TokenType_WHILE,//
   TokenType_RAVNO//
} TokenType;


typedef struct __Token Token;
struct __Token {
   TokenType type;
    char * name;
};

Token * Token_new(TokenType type,  char * name);

void Token_free(Token * self);

bool Token_equals(Token * self, Token * other);

void TokenType_print(TokenType type);

int Lexer_splitTokens(const char * input, List * tokens);

void Lexer_clearTokens(List * tokens);

void Lexer_printTokens(List * tokens);

const char *TokenType_toString(TokenType type);





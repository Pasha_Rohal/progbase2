#include <stdlib.h>
#include <stdio.h>
#include <fs.h>
#include <list.h>
#include <lexer.h>
#include <stdbool.h>
#include <parser.h>
#include <ast.h>
#include <string.h>
#include <string_buffer_ext.h>


static void PrintPretty(Tree *node, const char *indent, int root, int last);
static char *str_append(const char *str, const char *append);


void AstTree_prettyPrint(Tree *astTree);

/*
int main(void)
{
    //
    /*
    chr *fileName = "../main.bol";
    size_t len = getFileSize(fileName);
    char input[len + 1];
    readFileToBuffer(fileName, input, len + 1);
    input[len] = 0;
    puts("================");
    List *tokens = List_new();
    if (0 != Lexer_splitTokens(input, tokens))
    {
        Lexer_clearTokens(tokens);
        List_free(tokens);
        return EXIT_FAILURE;
    }
    Lexer_printTokens(tokens);
    puts("\n================");
    Tree *root = Parser_buildNewAstTree(tokens);
    //
    AstTree_prettyPrint(root);
    puts("==================");
    // mm leak
Lexer_clearTokens(tokens);
AstNode_free(root->iterue);
Tree_free(root);
List_free(tokens);
    return 0;
}
*/

int main(void){
FILE * f = freopen("../main_tokens.txt", "w", stdout);
FILE *fp = fopen("../main.bol", "r");

if (fp == NULL)
{
printf("Cannot open file.\n");
return EXIT_FAILURE;
}
char iter[getFileSize("../main.bol")];
int nread;
fseek(fp,0,0);
nread = readFileToBuffer("../main.bol", iter, getFileSize("../main.bol"));
iter[nread] = '\0';
fclose(fp);

List *tokens = List_new();
Lexer_splitTokens(iter, tokens);
Lexer_printTokens(tokens);
Tree *root = Parser_buildNewAstTree(tokens);
AstTree_prettyPrint(root);
Lexer_clearTokens(tokens);
AstNode_free(root->value);
Tree_free(root);
List_free(tokens);
fclose(f);
return 0;
}

void AstTree_prettyPrint(Tree *astTree)
{
    char *indent = strdup("");
    PrintPretty(astTree, indent, 1, 1);
    free((void *)indent);
}

static char *str_append(const char *str, const char *append)
{
    size_t newLen = strlen(str) + strlen(append) + 1;
    char *buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf, "%s%s", str, append);
    return buf;
}

static void PrintPretty(Tree *node, const char *indent, int root, int last)
{
    if (!node)
        return;

    printf("%s", indent);
    char *newIndent = NULL;
    if (last)
    {
        if (!root)
        {
            printf("└─");
            newIndent = str_append(indent, "◦◦");
        }
        else
        {
            newIndent = str_append(indent, "");
        }
    }
    else
    {
        printf("├─");
        newIndent = str_append(indent, "|");
    }
    AstNode *astNode = (node->value);
    printf("%s\n", astNode->name);
    List *children = (node->children);
    size_t count = List_count(children);
    for (int i = 0; i < count; i++)
    {
        void *child = List_at(children, i);
        PrintPretty(child, newIndent, 0, i == count - 1);
    }
    free((void *)newIndent);
}
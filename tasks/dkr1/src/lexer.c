#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include <iterator.h>
#include <list.h>
#include <lexer.h>

Token *Token_new(TokenType type, char *name){
    Token *tokens = malloc(sizeof(Token));
    tokens->type = type;
    tokens->name = malloc(strlen(name) + 1);
    strncpy(tokens->name, name, strlen(name));
    tokens->name[strlen(name)] = 0;
    return tokens;
}

void Token_free(Token *self){
    free(self->name);
    free(self);
}

static const char *operatorSpell[] = {
"+",
"-",
"%",
"/",
"{",
"}",
"[",
"]",
")",
"(",
";",
"**",
",",
"==",
"&&",
"&",
"!",
"!=",
"<=",
"<",
">",
">=",
"string",
"RED",
"if",
"else",
"bool",
"ID",
"Number",
"*",
"||",
"while",
"=",};



const char *TokenType_toString(TokenType type){
return operatorSpell[type];
}


void Lexer_printTokens(List *tokens){
    Iterator * begin = List_getNewBeginIterator(tokens);
    Iterator * end = List_getNewEndIterator(tokens);
    while(!Iterator_equals(begin,end)){
        Token * token =Iterator_value(begin);
        if (token->type == TokenType_NUMBER){
            printf("<NUM,%s> ", token->name);
        }
        else if (token->type == TokenType_BOOL){
            printf("<BOOL,%s> ", token->name);
        }
        else if (token->type == TokenType_ID){
            printf("<ID,%s> ", token->name);
        }
        else if (token->type == TokenType_STRING){
            printf("<STR,%s> ", token->name);
        }
        else{
            printf("<%s> ", token->name);
        }
        Iterator_next(begin); 
    }
    puts(" ");
    Iterator_free(begin);
    Iterator_free(end);
}



int Lexer_splitTokens(const char *input, List *tokens){
    int count = 0;
    char * starts;
    while (1)
    {
        if (*input == '\0'){
            break;
        }
        else if(*input == ' '){
            input++;
            input--;
        }
        else if (*input == '+'){
            List_add(tokens, Token_new(TokenType_PLUS, "+"));
        }
        else if (*input == '-'){
            List_add(tokens, Token_new(TokenType_MINUS, "-"));
        }
        else if(*input == '/'){
            List_add(tokens,Token_new(TokenType_DILENYA,"/"));
        }
        else if(*input == '{'){
            List_add(tokens,Token_new(TokenType_BLOCKOPEN,"{"));
        }
        else if(*input == '}'){
            List_add(tokens,Token_new(TokenType_BLOCKCLOSE,"}"));
        }
        else if(*input == '['){
            List_add(tokens,Token_new(TokenType_ARRAYOPEN,"["));
        }
        else if(*input == ']'){
            List_add(tokens,Token_new(TokenType_ARRAYCLOSE,"]"));
        }
        else if(*input == '('){
            List_add(tokens,Token_new(TokenType_OPERETOPEN,"("));
        }
        else if(*input == ')'){
            List_add(tokens,Token_new(TokenType_OPERETCLOSE,")"));
        }
        else if(*input == ';'){
            List_add(tokens,Token_new(TokenType_ENDLINEPROGRAME,";"));
        }
        else if(*input == '%'){
            List_add(tokens,Token_new(TokenType_PERSENT,"%"));
        }
        else if(*input == ','){
            List_add(tokens,Token_new(TokenType_COMMA,","));
        }
        else if (*input== '*'){
            if (*(input + 1) == '*'){
                List_add(tokens, Token_new(TokenType_DOUBLEPOW, "**"));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_POW, "*"));
            }
        }
        else if (*input== '|'){
            if (*(input + 1) == '|'){
                List_add(tokens, Token_new(TokenType_OR, "||"));
                input++;
            }
        }
        else if (*input == '&'){
            if (*(input + 1) == '&'){
                List_add(tokens, Token_new(TokenType_AND, "&&"));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_AMPERSANT  , "&"));
                input++;
            }
        }
        else if (*input == '='){
            if (*(input + 1) == '='){
                List_add(tokens, Token_new(TokenType_RAVENSTVO, "=="));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_RAVNO, "="));
            }
        }
        else if (*input == '!'){
            if (*(input + 1) == '='){
                List_add(tokens, Token_new(TokenType_NOT_RAVNO, "!="));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_OTRITSANIE, "!"));
            }
        }
        else if (*input == '<'){
            if (*(input + 1) == '='){
                List_add(tokens, Token_new(TokenType_MENSHERAVNO, "<="));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_MENSHE, "<"));
            }
        }
        else if (*input == '>'){
            if (*(input + 1) == '='){
                List_add(tokens, Token_new(TokenType_BOLSHERAVNO, ">="));
                input++;
            }
            else{
                List_add(tokens, Token_new(TokenType_BOLSHE, ">"));
            }
        }
        else if (*input == 'c' && *(input + 1) == 'o' && *(input + 2) == 'u' && *(input + 3) == 'n' && *(input + 4) == 't'){
            List_add(tokens, Token_new(TokenType_ID, "count")); //strlen
            input += 4;
        }
        else if (*input == 'a' && *(input + 1) == 't'){
            List_add(tokens, Token_new(TokenType_ID, "at")); //at
            input++;
        }
        else if (*input == 'p' && *(input + 1) == 'i' && *(input + 2) == 'c' && *(input + 3) == 'a' && *(input + 4) == 't'){
            List_add(tokens, Token_new(TokenType_ID, "picat")); // print
            input += 4;
        }
        else if (*input == 'b' && *(input + 1) == 'e' && *(input + 2) == 'r' && *(input + 3) == 'y'){
            List_add(tokens, Token_new(TokenType_ID, "bery"));  //scanf
            input += 3;
        }
        else if (*input == 'r' && *(input + 1) == 'e' && *(input + 2) == 'd'){
            List_add(tokens, Token_new(TokenType_RED, "red")); //led == red
            input += 2;
        }
        else if (*input == 'i' && *(input + 1) == 'f'){
            List_add(tokens, Token_new(TokenType_IF, "if")); // if
            input++;
        }
        else if (*input == 'e' && *(input + 1) == 'l' && *(input + 2) == 's' && *(input + 3) == 'e'){
            List_add(tokens, Token_new(TokenType_ELSE, "else")); //else
            input += 3;
        }
        else if (*input == 'p' && *(input + 1) == 'r' && *(input + 2) == 'a' && *(input + 3) == 'v' && *(input + 4) == 'd' && *(input+5) == 'a'){
            List_add(tokens, Token_new(TokenType_BOOL, "pravda"));//true
            input += 5;
        }
        else if (*input == 'd' && *(input + 1) == 'i' && *(input + 2) == 'c' && *(input + 3) == 'h'){
            List_add(tokens, Token_new(TokenType_BOOL, "dich"));//false
            input += 3;
        }
        else if (*input == 'k' && *(input + 1) == '0' && *(input + 2) == 'r' && *(input + 3) == 'n'){
            List_add(tokens, Token_new(TokenType_ID, "korn")); //sqrt
            input += 3;
        }
        else if (*input == 'p' && *(input + 1) == 'o' && *(input + 2) == 'w'){
            List_add(tokens, Token_new(TokenType_ID, "pow")); // ** or pow
            input += 2;
        }
        else if (*input == 'd' && *(input + 1) == 'l' && *(input + 2) == 'i' && *(input + 3) == 'n' && *(input + 4) == 'a'){
            List_add(tokens, Token_new(TokenType_ID, "dlina")); //strlen
            input += 4;
        }
        else if (*input == 's' && *(input + 1) == 'u' && *(input + 2) == 'b' && *(input + 3) == 's' && *(input + 4) == 't' && *(input + 5) == 'r'){
            List_add(tokens, Token_new(TokenType_ID, "substr")); //substr
            input += 5;
        }
        else if (*input == 'w' && *(input + 1) == 'h' && *(input + 2) == 'i' && *(input + 3) == 'l' && *(input + 4) == 'e'){
            List_add(tokens, Token_new(TokenType_WHILE, "while")); // while
            input += 4;
        }
        else if (*input == '"') //check first
        {
            int cheking = 0;
            int string = -1;
            input++; 
            char * lited = (char *)input;
            while ( string != cheking)
            {
                if (*input == '"'){ //second check
                    string = cheking;
                }
                if (count == cheking) {
                    starts = lited;
                }                    //exit while
                count++;
                input++;
            }
            char boolOff[count];
            strncpy(boolOff, starts, count);
            boolOff[count] = cheking;
            if(boolOff[count] == cheking){
            count = cheking;
            input--;
            if(count!=cheking){
                    printf("count ist check");
                }
                //STRING
            List_add(tokens, Token_new(TokenType_STRING, boolOff));
            }else{
                printf("count is not 0");
            }
        }
        else
        {
            char * lited = (char *)input;
            if (isdigit(*input)){
                bool qeenNumber = false;
                while (isdigit(*input) || (!qeenNumber && *input == '.')){ // check drob numbers
                    if (*input == '.')
                        qeenNumber = true;
                    if (count == 0) {
                        starts = lited;
                    }
                    count++;
                    input++;
                }
                int cheking = 0;
                char boolOff[count + 1];
                strncpy(boolOff, starts, count);
                boolOff[count] = cheking;
                if(boolOff[count] == cheking){
                count = cheking;
                input--;
                if(count!=cheking){
                    printf("count ist check");
                }
                //NUM
                List_add(tokens, Token_new(TokenType_NUMBER, boolOff));
                }else{
                    printf("count is not 0");
                }
            }
            else if (isalpha(*input)){
                int cheking = 0;
                char * lited = (char *)input;
                while (isalpha(*input)){

                    if (count == cheking) {
                        starts = lited;
                    }
                    count++;
                    input++;
                }
                char boolOff[count + 2];
                strncpy(boolOff, starts, count);
                boolOff[count] = cheking;
                if(boolOff[count] == cheking){
                count = cheking;
                input--;
                if(count!=cheking){
                    printf("count isnt check");
                }
                //ID
                List_add(tokens, Token_new(TokenType_ID, boolOff));
                }else{
                    printf("count is not 0");
                }
            }
        }
        input++;
    }
    count = 0;
    return 0;
}

void Lexer_clearTokens(List *tokens) {
    if (tokens == NULL) {
        printf("NULL");
    }
    for (int i = 0; i < List_count(tokens); i++) {
        Token_free(List_at(tokens, i));
    }
}
bool Token_equals(Token *self, Token *other){
    if (self->type == other->type){
        if (strcmp(self->name, other->name) == 0){
            return true;
        }
    }
    return false;
}



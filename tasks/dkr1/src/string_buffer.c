#include <string_buffer.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string_buffer_ext.h>


struct __StringBuffer 
{
    char* array;
    size_t capacity;
    size_t length;
};

static const size_t  INITIAL_CAPACITY = 256;

StringBuffer * StringBuffer_new(void)
{
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->array = malloc(sizeof(char) * self->capacity);    
    StringBuffer_clear(self);
    
    return self;
}

void StringBuffer_free(StringBuffer * self)
{
    free(self->array);
    free(self);
}

static void ensureCapacity(StringBuffer *self, size_t appendLength)
{
    while(self->length + appendLength > self->capacity)
    {
        size_t newCapacity = self->capacity * 2;
        char *newMem = realloc(self->array, sizeof(char) * newCapacity);
        if(newMem == NULL)
       {
            printf("Error");            //@todo error
        }
        self->array = newMem;
        self->capacity = newCapacity;
    }
}


void StringBuffer_append(StringBuffer * self, const char * str)
{
    size_t strLen = strlen(str);
    ensureCapacity(self, strLen);
    strcat(self->array + self->length - 1, str);
    self->length += strLen;
    //@todo
}

void StringBuffer_appendChar(StringBuffer * self, char ch)
{
    //todo check and realloc
    ensureCapacity(self, 1);    
    self->array[self->length - 1] = ch;
    self->array[self->length] = '\0';  
    self->length++;  
}

void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...)
{
    va_list vlist;
    va_start(vlist, fmt);
    ssize_t bufsz = vsnprintf(NULL,0,fmt,vlist);
    char * buf = malloc(bufsz + 1);
    va_start(vlist, fmt);
    vsnprintf(buf,bufsz + 1,fmt,vlist);
    va_end(vlist);
    StringBuffer_append(self,buf);
    free(buf);
}

void StringBuffer_clear(StringBuffer * self)
{
    self->array[0] = '\0';
    self->length = 1;    
}


char * StringBuffer_toNewString(StringBuffer * self)
{
    char * str = self->array;
    return strdup(str);
}


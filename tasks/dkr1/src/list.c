#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <list.h>


struct __List {
    void * items[1000];
    size_t length;
};

List * List_new(void)
{
    List * self = malloc(sizeof(List));
    if(self == NULL){
        printf("NULL");
    }
    self->length = 0;
    return self;
}

void List_free(List * self)
{
    free(self);
}

void List_add(List * self, void * value)
{
    if(self == NULL){
        printf("Null pointer exeption");
        abort();

    }else{
    self->items[self->length] = value;
    self->length++;
    }
}

void List_insert(List * self, size_t index, void * value)
{
    if(index < 0){
        printf("invalid index");
    }
    if(self == NULL){
        printf("Out of memory");
        abort();
    }
    for (int i = self->length; i > index; i--) {
        self->items[i] = self->items[i - 1];
    }
    self->items[index] = value;
    self->length++;
}

void * List_removeAt(List * self, size_t index)
{
    if(index < 0){
        printf("index been very low");
    }
    if(self == NULL){
        printf("Null pointer exeption");
        abort();
    }
    assert(index < self->length);
    void * val = self->items[index];
    for (int i = index + 1; i < self->length; i++) {
        self->items[i - 1] = self->items[i];
    }
    self->length--;
    return val;
}

size_t List_count(List * self)
{
    return self->length ;
}

void * List_at(List * self, size_t index)
{
    return self->items[index];
}

//iterator


struct __Iterator {
    List *list;
    int index;
};

static Iterator *Iterator_new(List *list,int index){
    Iterator *self = malloc(sizeof(Iterator));
    self->list=list;
    self->index=index;
    return self;
}
Iterator * List_getNewBeginIterator(List * self){
    return Iterator_new(self,0);
}

Iterator * List_getNewEndIterator(List * self){
    int lastIndex = List_count(self);
    return Iterator_new(self,lastIndex);
}


void * Iterator_value(Iterator * self){
    assert(self->index < List_count(self->list));
    return List_at(self->list,self->index);
}

void Iterator_free(Iterator * self){
    free(self);
}

void Iterator_next(Iterator * self){
    self->index++;
}

void Iterator_prev(Iterator * self){
    self->index--;
}

void Iterator_advance(Iterator * self,IteratorDistance n){
    self->index=+n;
}

bool Iterator_equals(Iterator * self,Iterator * other){
     return self->list == other->list
     && self->index == other->index;
}

IteratorDistance Iterator_Distance(Iterator * begin,Iterator * end){
    return end->index-begin->index;
}

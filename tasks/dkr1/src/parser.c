#include <list.h>
#include <parser.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <lexer.h>
#include <string_buffer.h>
#include <string_buffer_ext.h>
#include <ast.h>

#define TRACE_CALL()\
    parser->level++;\
    trace(parser, __func__);\
    Parser *_parserPtr __attribute__((cleanup(Parser_decLevel))) = parser;

#define TRACE_EXPECT(TYPE)\
    traceExpect(parser, TYPE);


typedef struct
{
    Iterator *tokens;
    Iterator *tokensEnd;
    char *error;
    int level;
} Parser;

// check current token type
static Tree *accept(Parser *self, TokenType tokenType);
// expect exactly that token type or fail
static Tree *expect(Parser *self, TokenType tokenType);

static Tree *prog(Parser *parser);
static Tree *var_decl(Parser *parser);
static Tree *st(Parser *parser);
static Tree *ID(Parser *parser);
static Tree *expr(Parser *parser);
static Tree *expr_st(Parser *parser);
static Tree *block_st(Parser *parser);
static Tree *select_st(Parser *parser);
static Tree *iter_st(Parser *parser);
static Tree *assign(Parser *parser);
static Tree *assign_ap(Parser *parser);
static Tree *log_or(Parser *parser);
static Tree *log_or_ap(Parser *parser);
static Tree *log_and(Parser *parser);
static Tree *log_and_ap(Parser *parser);
static Tree *eq(Parser *parser);
static Tree *eq_ap(Parser *parser);
static Tree *rel(Parser *parser);
static Tree *rel_ap(Parser *parser);
static Tree *add(Parser *parser);
static Tree *add_ap(Parser *parser);
static Tree *mult(Parser *parser);
static Tree *mult_ap(Parser *parser);
static Tree *unary(Parser *parser);
static Tree *primary(Parser *parser);
static Tree *NUMBER(Parser *parser);
static Tree *STRING(Parser *parser);
static Tree *var_or_call(Parser *parser);
static Tree *parentheses(Parser *parser);
static Tree *fn_call(Parser *parser);
static Tree *arg_list(Parser *parser);

//
Tree *Parser_buildNewAstTree(List *tokens)
{
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1};
    Tree *progNode = prog(&parser);
    if (parser.error)
    {
        fprintf(stderr, ">>> ERROR: %s\n", parser.error);
        free((void *)parser.error);
        return false;
    }
    else{
        if (!Iterator_equals(parser.tokens, parser.tokensEnd)){
            Token *token = Iterator_value(parser.tokens);
            fprintf(stderr, ">>> ERROR: unexpected token '%s' .\n", parser.error);
        }
    }
    
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);
    return progNode;
}

static bool eoi(Parser *self){
    return Iterator_equals(self->tokens, self->tokensEnd);
}

void Parser_decLevel(Parser **parserPtr){
    (*parserPtr)->level--;
}
static void traceLevel(int level){
    for (int i = 0; i < level; i++){
        putchar('.');
        putchar('.');
    }
}

static void traceExpect(Parser *parser, TokenType expectedType){
    traceLevel(parser->level);
    printf("<%s>\n", TokenType_toString(expectedType));
}

static void trace(Parser *parser, const char *fn){
    traceLevel(parser->level);
    if (eoi(parser)){
        printf("%s: <EOI>\n", fn);
        return;
    }
    Token *token = Iterator_value(parser->tokens);
TokenType type = token->type;
    switch (type){
    case TokenType_ID: case TokenType_NUMBER: case TokenType_STRING:{
        printf("%s: <%s,%s>\n", fn, TokenType_toString(type), token->name);
        break;
    }
    default:{
        printf("%s: <%s>\n", fn, TokenType_toString(type));
        break;
        }
    }
}


static AstNodeType TokenType_toAstNodeType(TokenType type){
    switch (type){
    case TokenType_RAVNO:return AstNodeType_ASSIGN;
    case TokenType_PLUS:return AstNodeType_ADD;
    case TokenType_MINUS:return AstNodeType_SUB;
    case TokenType_POW:return AstNodeType_MUL;
    case TokenType_DILENYA:return AstNodeType_DIV;
    case TokenType_PERSENT:return AstNodeType_MOD;
    case TokenType_RAVENSTVO:return AstNodeType_EQ;
    case TokenType_NOT_RAVNO:return AstNodeType_NEQ;
    case TokenType_OTRITSANIE:return AstNodeType_NOT;
    case TokenType_MENSHE:return AstNodeType_LT;
    case TokenType_BOLSHE:return AstNodeType_GT;
    case TokenType_MENSHERAVNO:return AstNodeType_LTEQ;
    case TokenType_BOLSHERAVNO:return AstNodeType_GTEQ;
    case TokenType_AND:return AstNodeType_AND;
    case TokenType_OR:return AstNodeType_OR;
    //
    case TokenType_ID:return AstNodeType_ID;
    case TokenType_NUMBER:return AstNodeType_NUMBER;
    case TokenType_STRING:return AstNodeType_STRING;
    //
    default:
        return AstNodeType_UNKNOWN;
    }
}

static bool acceptcheck(Parser * self,TokenType tokenType){
    Tree * freeLs = accept(self,tokenType);
    if(freeLs){
        AstNode_free(freeLs->value);
        Tree_free(freeLs);
        return true;
    }
    return false;

}

static bool expectcheck(Parser * self,TokenType tokenType){
    Tree * freeLs = accept(self,tokenType);
    if(freeLs){
        AstNode_free(freeLs->value);
        Tree_free(freeLs);
        return true;
        
    }
    return false;

}

static Tree *accept(Parser *self, TokenType tokenType){
    if (eoi(self))
        return false;
    Token *lexeme = Iterator_value(self->tokens);
    if (lexeme->type == tokenType){
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char *astName = strdup((char *)lexeme->name);
        AstNode *node = AstNode_new(astType, astName);
        Tree *tree = Tree_new(node);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}

static Tree *expect(Parser *parser, TokenType tokenType){
    Tree *tree = accept(parser, tokenType);
    if (tree != NULL){
        TRACE_EXPECT(tokenType);
        return tree;
    }
    const char *currentTokenType = eoi(parser)
        ? "end-of-input"
        : TokenType_toString(((Token *)Iterator_value(parser->tokens))->type);

    StringBuffer *sb = StringBuffer_new();
    StringBuffer_appendFormat(sb, "expected '%s' got '%s'.\n", TokenType_toString(tokenType), currentTokenType);
    parser->error = StringBuffer_toNewString(sb);

    StringBuffer_free(sb);
    return NULL;
}

// ebnf

typedef Tree *(*GrammarRule)(Parser *parser);

static bool ebnf_multiple(Parser *parser, List *nodes, GrammarRule rule){
    Tree *node = NULL;
    while ((node = rule(parser)) && !parser->error){
        List_add(nodes, node);
    }
    return parser->error == NULL ? true : false;
}

static Tree *ebnf_select(Parser *parser, GrammarRule rules[], size_t rulesLen){
    Tree *node = NULL;
    for (int i = 0; i < rulesLen && !node; i++){
        GrammarRule rule = rules[i];
        node = rule(parser);
        if (parser->error)
            return NULL;
    }
    return node;
}

static Tree *ebnf_selectLexemes(Parser *parser, TokenType types[], size_t typesLen){
    Tree *node = NULL;
    for (int i = 0; i < typesLen && !node; i++){
        node = accept(parser, types[i]);
    }
    return node;
}

static Tree *ebnf_ap_main_rule(Parser *parser, GrammarRule next, GrammarRule ap){
    Tree *nextNode = next(parser);
    if (nextNode){
        Tree *apNode = ap(parser);
        if (apNode){
            List_insert(apNode->children, 0, nextNode);
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}
static Tree *ebnf_ap_recursive_rule(Parser *parser, TokenType types[], size_t typesLen, GrammarRule next, GrammarRule ap){
    Tree *opNode = ebnf_selectLexemes(parser, types, typesLen);
    if (opNode == NULL)
        return NULL;
    Tree *node = NULL;
    Tree *nextNode = next(parser);
    if(!nextNode){
        return NULL;
    }
    Tree *apNode = ap(parser);
    if (apNode){
        List_insert(apNode->children, 0, nextNode);
        node = apNode;
    }
    else{
        node = nextNode;
    }
    List_add(opNode->children, node);
    return opNode;
}


static Tree *ID(Parser *parser){
    TRACE_CALL();
    return accept(parser, TokenType_ID);
}
static Tree *NUMBER(Parser *parser){
    TRACE_CALL();
    return accept(parser, TokenType_NUMBER);
}
static Tree *STRING(Parser *parser){
    TRACE_CALL();
    return accept(parser, TokenType_STRING);
}

static Tree *ID_expect(Parser *parser){
    return expect(parser, TokenType_ID);
}

static Tree *prog(Parser *parser){
    TRACE_CALL();
    Tree *progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, "program"));
    (void)ebnf_multiple(parser, progNode->children, var_decl);
    (void)ebnf_multiple(parser, progNode->children, st);
    return progNode;
}
static Tree *var_decl(Parser *parser){
    TRACE_CALL();
    if (!acceptcheck(parser, TokenType_RED))
        return NULL;
    Tree *idNode = ID_expect(parser);
    if (!idNode)
        return NULL;
    if (!expectcheck(parser, TokenType_RAVNO)){
        printf("ID_expect ERROR");
        return NULL;
    }
    Tree *exprNode = expr(parser);
    if (exprNode == NULL){
        printf("ID_expect ERROR");
        //TODO error
        return NULL;
    }
    if (!expectcheck(parser, TokenType_COMMA)){
        printf("EXPECT ERROR");
        return NULL;
    }
    Tree *varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, "declareVar"));
    List_add(varDecl->children, idNode);
    List_add(varDecl->children, exprNode);
    return varDecl;
}
static Tree *st(Parser *parser){
    TRACE_CALL();
    return ebnf_select(parser,
        (GrammarRule[]){
        block_st,
        select_st,
        iter_st,
        expr_st},
        4);
}
static Tree *expr(Parser *parser){
    TRACE_CALL();
    return assign(parser);
}
static Tree *expr_st(Parser *parser){
    TRACE_CALL();
    Tree *exprNode = expr(parser);
    if (exprNode){
        expect(parser, TokenType_COMMA);
    }
    else{
        accept(parser, TokenType_COMMA);
    }
    return exprNode;
}
static Tree *block_st(Parser *parser){
    TRACE_CALL();
    if (!acceptcheck(parser, TokenType_BLOCKOPEN))
        return NULL;
    Tree *blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, "block"));
    if (ebnf_multiple(parser, blockNode->children, st)){
        (void)expect(parser, TokenType_BLOCKCLOSE);
    }
    return blockNode;
}
static Tree *select_st(Parser *parser){
    TRACE_CALL();

    if (!acceptcheck(parser, TokenType_IF) || !expect(parser, TokenType_OPERETOPEN))
        return NULL;
    Tree *testExprNode = expr(parser);
    if (!testExprNode){
        printf("ERROR");
        // TODO error
        free(testExprNode);
        return NULL;
    }
    if (!expectcheck(parser, TokenType_OPERETCLOSE)){
        // TODO clear testExpr
        // TODO error
        printf("ERROR");
        return NULL;
    }
    Tree *stNode = st(parser);
    if (stNode == NULL){
        printf("ERROR");
        free(stNode);
        // TODO clear testExpr
        // TODO error
        return NULL;
    }
    Tree *ifNode = Tree_new(AstNode_new(AstNodeType_IF, "if"));
    List_add(ifNode->children, testExprNode);
    List_add(ifNode->children, stNode);

    if (acceptcheck(parser, TokenType_ELSE)){
        Tree *elseNode = st(parser);
        if (elseNode == NULL || parser->error){
            // @todo error
            return NULL;
        }
        List_add(ifNode->children, stNode);
    }
    return ifNode;
}
static Tree *iter_st(Parser *parser){
    TRACE_CALL();
    if (!acceptcheck(parser, TokenType_WHILE) || !expect(parser, TokenType_OPERETOPEN))
        return NULL;
    Tree *testExprNode = expr(parser);
if (!testExprNode){
        // @TODO error
        return NULL;
    }
    if (!expectcheck(parser, TokenType_OPERETCLOSE)){
        // TODO clear testExpr
        // TODO error
        return NULL;
    }
    Tree *stNode = st(parser);
    if (stNode == NULL){
        // TODO clear testExpr
        // TODO error
        return NULL;
    }
    Tree *whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, "while"));
    List_add(whileNode->children, testExprNode);
    List_add(whileNode->children, stNode);
    return whileNode;
}
static Tree *assign(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_or, assign_ap);
}
static Tree *assign_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_RAVNO},
        1,
        log_or, assign_ap);
}
static Tree *log_or(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_and, log_or_ap);
}
static Tree *log_or_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_OR},
        1,
        log_and, log_or_ap);
}
static Tree *log_and(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, eq, log_and_ap);
}
static Tree *log_and_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_AND},
        1,
        eq, log_and_ap);
}
static Tree *eq(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, rel, eq_ap);
}
static Tree *eq_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_RAVENSTVO,
        TokenType_NOT_RAVNO},
        2,
        rel, eq_ap);
}
static Tree *rel(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, add, rel_ap);
}
static Tree *rel_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_MENSHE,
        TokenType_MENSHERAVNO,
        TokenType_BOLSHE,
        TokenType_BOLSHERAVNO},
        4,
        add, rel_ap);
}
static Tree *add(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, mult, add_ap);
}
static Tree *add_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_PLUS,
        TokenType_MINUS},
        2,
        mult, add_ap);
}
static Tree *mult(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, unary, mult_ap);
}
static Tree *mult_ap(Parser *parser){
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser,
        (TokenType[]){
        TokenType_POW,
        TokenType_DILENYA,
        TokenType_PERSENT},
        3,
        unary, mult_ap);
}

static Tree *unary(Parser *parser){
    TRACE_CALL();
    Tree *opNode = ebnf_selectLexemes(parser,
        (TokenType[]){
        TokenType_PLUS,
        TokenType_MINUS,
        TokenType_OTRITSANIE},
3);
    Tree *primNode = primary(parser);
    if (opNode){
        List_add(opNode->children, primNode);
        return opNode;
    }
    return primNode;
}

static Tree *primary(Parser *parser){
    TRACE_CALL();
    return ebnf_select(parser,
        (GrammarRule[]){
        NUMBER,
        STRING,
        var_or_call,
        parentheses},
        4);
}
static Tree *var_or_call(Parser *parser){
    TRACE_CALL();
    Tree *varNode = ID(parser);
    Tree *argListNode = fn_call(parser);
    if (argListNode){
        List_add(varNode->children, argListNode);
    }
    return varNode;
}
static Tree *parentheses(Parser *parser){
    TRACE_CALL();
    if (!acceptcheck(parser, TokenType_OPERETOPEN))
        return NULL;
    Tree *exprNode = expr(parser);
    expect(parser, TokenType_OPERETCLOSE);
    return exprNode;
}
static Tree *fn_call(Parser *parser){
    TRACE_CALL();
    if (!acceptcheck(parser, TokenType_OPERETOPEN))
        return NULL;
    Tree *argListNode = arg_list(parser);
    expect(parser, TokenType_OPERETCLOSE);
    return argListNode;
}
static Tree *arg_list(Parser *parser){
    TRACE_CALL();
    Tree *exprNode = expr(parser);
    Tree *argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, "arglist"));
    if (exprNode){
        List_add(argListNode->children, exprNode);
        while (true){
            if (!accept(parser, TokenType_COMMA))
                break;
            exprNode = expr(parser);
            if (exprNode){
                List_add(argListNode->children, exprNode);
            }
            else{
                break;
            }
        }
    }
    return argListNode;
}
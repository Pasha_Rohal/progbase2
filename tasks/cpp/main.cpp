#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <list>
#include <limits>


#include <point2d.h>

using namespace std;

int main(void){
    list<Point2D> point;
    while(1){
        cout <<endl;
        cout << "Enter number function" << endl;
        cout << "1. Print" << endl;
        cout << "2. Add new element" << endl;
        cout << "3. Print all Pointer less then number K" << endl;
        cout << "4. Exit" << endl;
        int ch;
        cin >> ch;
        while (ch <= 0) {
            cin >> ch;
            if (cin.fail()){
                cin.clear();
                cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
        switch(ch){
            case 1:
            {
                for(auto val : point){
                    val.Print();
                }
                cout << "------" << endl;
                break;
            }
            case 2:
            {
                cout << " name : ";
            int x;
            int y;
            string name;
            cin >> name;
            cout << endl
             << " x : ";
            cin >> x;
            while (x <= 0) {
            cin >> x;
                if (cin.fail()){
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
            cout << endl
                 << " y : ";
            cin >> y;
            while (y <= 0) {
                cin >> y;
                if (cin.fail()){
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
            Point2D p(x, y, name);
            point.push_back(p);
            cout << "------" << endl;
            break;
            }
            case 3:
            {
                cout << "Print K"<<endl;
                cout << " K :";
                int k;
                cin >> k;
                while (k <= 0) { 
                    cin >> k;
                    if (cin.fail()){
                        cin.clear();
                        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    }
                }
                for (auto val : point)
                {
                    if (val.vector() < k)
                    {
                        val.Print();
                    }
                }
            cout << "-----" << endl;
            break;
            }
            case 4:{
            return 0;
            }
        }
    }
    return 0;
}

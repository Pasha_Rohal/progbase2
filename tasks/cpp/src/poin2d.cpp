#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <math.h>
#include <point2d.h>


using namespace std;

Point2D :: Point2D(int x1,int y1,string name1){
    x = x1;
    y = y1;
    name = name1;
} 
double Point2D :: vector(){
    double l;
    l = pow(x,2) + pow(y,2);
    return sqrt(l);
}
void Point2D :: Print(){
    cout <<"----------" << endl;
    cout << "name " << name << endl;
    cout << "x " << x << endl;
    cout << "y " << y << endl << endl;
}